class Chat < ApplicationRecord
  has_many :users_chats, dependent: :destroy
  has_many :users, through: :users_chats
  belongs_to :owner, class_name: 'User'
  has_many :messages, dependent: :destroy

  accepts_nested_attributes_for :users_chats, allow_destroy: true, reject_if: :all_blank

  validates :name, presence: true
  validates :max_users, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
end
