class SystemSetting < ApplicationRecord
  validates :chats_count_for_user, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 1 }
  validates :users_count_for_chat, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 2 }
end
