class Message < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  belongs_to :user
  belongs_to :chat

  validates :chat_id, presence: true
  validates :user_id, presence: true
  validates :message, presence: true, length: { minimum: 1 }

  settings do
    mappings dynamic: false do
      indexes :message, type: :text, analyzer: :english
      indexes :chat_id, type: :integer
      indexes :user_id, type: :integer
    end
  end

  def self.search_in_chat(query, chat_id)
    self.search(
                query: {
                  bool: {
                    must: [
                      {
                        multi_match: {
                          query: query,
                          fields: [:message]
                        }
                      },
                      {
                        match: {
                          chat_id: chat_id
                        }
                      },
                    ]
                  }
                }
              )
  end
end
