class UsersChat < ApplicationRecord
  belongs_to :chat
  belongs_to :user

  validates_uniqueness_of :user_id, scope: :chat_id, message: 'This user already in the chat'
  validate :uniq_users

  private

  def uniq_users
    ids = self.chat.users_chats.map(&:user_id)

    if ids.select {|el| ids.count(el) > 1}.uniq.any?
      errors.add(:user_id, 'Users should be uniq')
    end
  end
end