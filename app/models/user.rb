class User < ApplicationRecord
  include Elasticsearch::Model
  include Elasticsearch::Model::Callbacks

  ### it is initial note ###
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable

  has_many :users_chats, dependent: :destroy
  has_many :chats, through: :users_chats
  has_many :owned_chats, class_name: 'Chat', foreign_key: :owner_id, dependent: :destroy
  has_many :messages
  has_many :blocked_users, class_name: 'BlockedUser', foreign_key: :user_id, dependent: :destroy

  enum role: [:user, :moderator]

  # validates :nickname, presence: true, length: { minimum: 2 }
  # validates :birthday, presence: true
  validate :birthday_in_past
  validates :max_chats, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: 0 }

  settings do
    mappings dynamic: false do
      indexes :nickname, type: :text
      indexes :email, type: :text
    end
  end

  def all_chats
    Chat.left_joins(:users)
        .where('users_chats.user_id = ? or chats.owner_id = ?', id, id)
        .distinct
  end

  def blocked_me_user_ids
    BlockedUser.where(blocked_user_id: id).map(&:user_id)
  end

  def blocked_user_ids
    blocked_users.map(&:blocked_user_id)
  end

  private

  def birthday_in_past
    if birthday.present? && birthday > Date.today
      errors.add(:birthday, "can't be in the future")
    end
  end
end
