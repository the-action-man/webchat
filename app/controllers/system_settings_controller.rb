class SystemSettingsController < ApplicationController
  before_action :authenticate_user!
  before_action :authenticate_moderator!
  before_action :set_system_setting, only: %i[edit update]

  def edit; end

  def update
    if @system_setting.update(system_settings_params)
      redirect_to :system_settings, notice: 'Settings was updated successfully'
    else
      render :edit
    end
  end

  private

  def set_system_setting
    @system_setting = SystemSetting.first
  end

  def system_settings_params
    params.require(:system_setting).permit(:chats_count_for_user, :users_count_for_chat)
  end

  def authenticate_moderator!
    unless current_user.moderator?
      redirect_to root_path, alarm: 'You are not a moderator!'
    end
  end
end
