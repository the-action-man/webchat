class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user, only: %i[show edit update]
  before_action :authenticate_moderator!, only: %i[manage apply_managing]

  def index
    @users = User.all.order(:id)

    if params.has_key? :search
      query_params = params.require(:search).permit(:q)
      query = query_params[:q]
      search_result = User.search(query)
      @founded_users_ids = search_result.map {|search| search.id.to_i}
    end
  end

  def edit; end

  def update
    birthday = user_params.delete(:birthday)
    if birthday.blank?
      @user.errors.add(:birthday, "Can't be blank")
      render :edit
      return
    end
    birthday = Date.parse(birthday)
    user_params[:birthday] = birthday

    if @user.update(user_params)
      redirect_to root_path, notice: 'Profile was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    current_user.destroy
    redirect_to root_path, notice: 'Account was successfully deleted.'
  end

  def manage
    @user = User.find(params[:id])
  end

  def apply_managing
    @user = User.find(params[:id])
    if @user.update(managed_user_params)
      redirect_to users_path, notice: 'New data for user is applied'
    else
      render :manage
    end
  end

  private

  def set_user
    @user = current_user
  end

  def user_params
    params.require(:user).permit(:nickname, :birthday, :no_invitation)
  end

  def managed_user_params
    result = params.require(:user).permit(:role, :max_chats).to_h
    result.each { |key, val| result[key] = val.to_i } # converting every str to int
  end

  def authenticate_moderator!
    unless current_user.moderator?
      redirect_to root_path, notice: 'You are not a moderator!'
    end
  end
end
