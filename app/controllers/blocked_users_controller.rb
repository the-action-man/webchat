class BlockedUsersController < ApplicationController
  before_action :authenticate_user!

  def index
    @blocked_users = current_user.blocked_users
  end

  def new
    @blocked_user = BlockedUser.new

    ids = current_user.blocked_users.pluck(:blocked_user_id).concat([current_user.id])
    @users = User.where.not(id: ids)
  end

  def add_user
    permitted_params = params.require(:blocked_user).permit(:blocked_user_id)
    @blocked_user = BlockedUser.new(permitted_params.merge(user_id: current_user.id))
    if @blocked_user.save
      redirect_to black_list_path, notice: 'User was added to black list.'
    end
  end

  def destroy
    set_blocked_user
    @blocked_user.destroy
    redirect_to black_list_path, notice: 'User is removed from black list.'
  end

  private

  def set_blocked_user
    if BlockedUser.exists?(params[:id])
      @blocked_user = BlockedUser.find(params[:id])
    else
      redirect_to black_list_path, alert: 'This user is absent in black list!'
    end
  end
end
