class ChatsController < ApplicationController
  before_action :set_chat, only: %i[show edit update destroy add_user users_list delete_user manage apply_managing]
  before_action :authenticate_user!
  before_action :authenticate_owner_or_moderator!, only: %i[destroy users_list add_user delete_user]
  before_action :authenticate_moderator!, only: %i[manage apply_managing]

  # GET /chats
  def index
    @allowed_to_create = allowed_to_creat_chat?
    @chats = policy_scope(Chat).order(:id)
  end

  # GET /chats/1
  def show
    @allowed_to_add_user = allowed_to_add_user?

    @users = @chat.users
    @owner = @chat.owner

    if params.has_key? :search
      query_params = params.require(:search).permit(:q)
      query = query_params[:q]
      search_result = Message.search_in_chat(query, @chat.id)
      @founded_messages_ids = search_result.map {|search| search.id.to_i}
    end

    if @owner.id == current_user.id || @users.ids.include?(current_user.id) || current_user.moderator?
      @messages = @chat.messages.order(id: :desc).limit(100).reverse
      unless @messages.present?
        flash[:notice] = "No one message in this chat. Type something... You will be first!"
      end
    else
      redirect_to chats_path, alert: 'You do not have access to this chat!'
    end
  end

  # GET /chats/new
  def new
    @chat = Chat.new
    @chat.users_chats.build
  end

  # GET /chats/1/edit
  def edit
    authorize @chat
  end

  # POST /chats
  def create
    allowed_to_create = allowed_to_creat_chat?

    @chat = Chat.new(chat_params.merge(owner_id: current_user.id))

    if allowed_to_create && @chat.save
      redirect_to @chat, notice: 'Chat was successfully created.'
    else
      if allowed_to_create
        errors = @chat.errors.full_messages.join(', ')
        @chat = Chat.new
        @chat.users_chats.clear
        @chat.users_chats.build
        render :new, notice: errors
      else
        redirect_to :chats, alert: @msg_not_allowed_to_creat_chat
      end
    end
  end

  # PATCH/PUT /chats/1
  def update
    authorize @chat
    if @chat.update(chat_params)
      redirect_to @chat, notice: 'Chat was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /chats/1
  def destroy
    authorize @chat
    @chat.destroy
    redirect_to chats_url, notice: 'Chat was successfully destroyed.'
  end

  def users_list
    @users = User.where.not(id: @chat.user_ids.concat([@chat.owner_id]))
                 .where.not(id: current_user.blocked_user_ids)
                 .where.not(id: current_user.blocked_me_user_ids)
                 .where(no_invitation: false)
  end

  def add_user
    @allowed_to_add_user = allowed_to_add_user?

    user_id = params[:user_id]
    if user_id.nil?
      message = 'You can\'t add empty user'
    elsif @chat.user_ids.include?(user_id)
      message = 'User already is a member of the chat'
    elsif !@allowed_to_add_user
      message = @msg_not_allowed_to_add_user
    else
      user = User.find(user_id)
      if user.no_invitation
        message = 'User cannot be added'
      else
        @chat.users << user
        message = 'User successfully added'
      end
    end

    redirect_to(@chat, notice: message)
  end

  def delete_user
    @user_id = params[:user_id]

    if @user_id.blank?
      message = 'You can\'t delete empty user'
    elsif @chat.user_ids.exclude?(@user_id.to_i)
      message = 'This user isn\'t a member of the chat'
    else
      @chat.users.delete(@user_id.to_i)
      message = 'You successfully delete user from the chat'
    end

    respond_to do |format|
      format.html { redirect_to @chat, notice: message }
      format.js
    end
  end

  def manage; end

  def apply_managing
    if @chat.update(managed_chat_params)
      redirect_to chats_path, notice: 'New data for chat is applied.'
    else
      render :manage
    end
  end

  def add_message
    chat_id = params[:id]
    message_params = new_message_params.merge(user_id: current_user.id, chat_id: chat_id)
    if message_params[:message].present?
      @message = Message.create(message_params)
    else
      @message = Message.new(message_params)
      @alert = "You can't send empty message"
    end

    MessageBroadcastJob.perform_later(@message, current_user.id)

    # respond_to do |format|
    #   format.html { redirect_to @chat, alert: @alert }
    #   format.js
    # end
  end

  def search
    query_params = params.require(:search).permit(:q)
    query = query_params[:q]
    @search_result = Message.search(query)

    unless current_user.moderator?
      user_chat_ids = current_user.all_chats.pluck(:id)
      @search_result = @search_result.select do |search|
        user_chat_ids.include?(search.chat_id)
      end
    end

    render :search_result
  rescue StandardError
    redirect_to :chats, alert: 'Sorry, search index is not ready. Try it a little bit later.'
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_chat
    if Chat.exists?(params[:id])
      @chat = Chat.find(params[:id])
    else
      redirect_to root_path, alert: 'This chat does not exist or wrong URL!'
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def chat_params
    params.require(:chat).permit(:name, users_chats_attributes: [:user_id, :id, :_destroy])
  end

  def new_message_params
    params.require(:message).permit(:message).to_h
  end

  def authenticate_owner_or_moderator!
    unless current_user.moderator? || current_user.id == @chat.owner_id
      redirect_to @chat, alert: 'You are not an owner!'
    end
  end

  def managed_chat_params
    result = params.require(:chat).permit(:max_users).to_h
    result.each { |key, val| result[key] = val.to_i } # converting every str to int
  end

  def authenticate_moderator!
    unless current_user.moderator?
      redirect_to root_path, alert: 'You are not a moderator!'
    end
  end

  def allowed_to_creat_chat?
    if current_user.moderator?
      allowed_to_create = true
    else
      quantity_of_allowed_chats = current_user.max_chats
      if quantity_of_allowed_chats == 0 # max chats value is not defined
        quantity_of_allowed_chats = SystemSetting.first.chats_count_for_user
      end
      user_chats_count = Chat.where(owner_id: current_user.id).count
      allowed_to_create = user_chats_count < quantity_of_allowed_chats
      @msg_not_allowed_to_creat_chat = "You are not allowed to create more than #{quantity_of_allowed_chats} chats!"
    end

    allowed_to_create
  end

  def allowed_to_add_user?
    if current_user.moderator?
      allowed_to_add = true
    else
      quantity_of_allowed_users = @chat.max_users
      if quantity_of_allowed_users == 0 # max users value is not defined
        quantity_of_allowed_users = SystemSetting.first.users_count_for_chat
      end
      users_in_chat = @chat.users.count + 1 # with owner
      allowed_to_add = users_in_chat < quantity_of_allowed_users
      @msg_not_allowed_to_add_user = "You are not allowed to add more than #{quantity_of_allowed_users} users!"
    end

    allowed_to_add
  end
end
