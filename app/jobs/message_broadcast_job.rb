class MessageBroadcastJob < ApplicationJob
  queue_as :default

  def perform(message, user_id)
    ActionCable.server.broadcast "chat:#{message.chat.id}",
                                 {
                                 message: message_view(message, user_id),
                                 chat_id: message.chat.id
                                 }

  end

  private

  def message_view(message, user_id)
    if message.user.id == user_id
      "<li class=\"list-group-item d-flex justify-content-between align-items-center\">
           #{message.message} <span class=\"badge badge-success badge-pill\">Me</span>
          </li>"
    else
      "<li class=\"list-group-item d-flex justify-content-between align-items-center\">
          #{message.message} <span class=\"badge badge-primary badge-pill\"> #{message.user.email}</span>
          </li>"
    end
  end
end
