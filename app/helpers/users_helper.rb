module UsersHelper
  def user_row_class(highlighted = false)
    base = ''

    if highlighted
      base + ' list-group-item-warning'
    else
      base
    end
  end
end
