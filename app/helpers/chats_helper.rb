module ChatsHelper
  def message_class(highlighted = false)
    base = "list-group-item d-flex justify-content-between align-items-center"

    if highlighted
      base + " list-group-item-warning"
    else
      base
    end
  end
end
