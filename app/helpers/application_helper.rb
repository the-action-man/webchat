module ApplicationHelper
  def user_presentation_text(user)
    email = user.email
    "(#{email})"
  end

  def user_presentation_class(user)
    if user.user?
      ''
    else
      'active'
    end
  end

  def message_owner(user)
    html = if user.blank?
             '<span class="badge badge-danger badge-pill">User is deleted</span>'
           elsif current_user.id == user.id
             '<span class="badge badge-success badge-pill">Me</span>'
           else
             "<span class=\"badge badge-primary badge-pill\"> #{user.email}</span>"
           end
    raw(html)
  end

  def alert_class(type)
    case type
    when 'alert'
      'danger'
    when 'success'
      'success'
    else
      'info'
    end
  end
end
