App.chats = App.cable.subscriptions.create "ChatsChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    active_chat = $("#chat_messages[data-chat-id='#{data.chat_id}']")
    if active_chat .length > 0
      active_chat.append(data.message)
    # Called when there's incoming data on the websocket for this channel
