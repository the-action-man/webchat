class ChatsChannel < ApplicationCable::Channel
  def subscribed
    current_user.all_chats.each do |chat|
      stream_from "chat:#{chat.id}"
    end
  end

  def unsubscribed
    stop_all_streams
  end
end
