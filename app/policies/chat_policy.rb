class ChatPolicy < ApplicationPolicy
  attr_reader :user, :chat

  def initialize(user, chat)
    @user = user
    @chat = chat
  end

  def show?
    #TODO: need implement
    true
  end

  def create?
    #TODO: need implement
    true
  end

  def edit?
    update?
  end

  def update?
    user.id == chat.owner_id || user.moderator?
  end

  def destroy?
    update?
  end

  def add_user?
    #TODO: need implement
    true
  end

  def users_list?
    #TODO: need implement
    true
  end

  class Scope < Scope
    def resolve
      if user.moderator?
        scope.all
      else
        scope.left_joins(:users)
            .where('users_chats.user_id = ? or chats.owner_id = ?', user.id, user.id)
            .distinct
      end
    end
  end
end
