# My Web chat project 


Technologies:

* Ruby version 2.6.1p33 (2019-01-30 revision 66950)

* Rails version 5.2.3

* Database: psql (PostgreSQL) 9.5.16

* Frontend: bootstrap 4.3.1

* ElasticSearch version 6.7.2
