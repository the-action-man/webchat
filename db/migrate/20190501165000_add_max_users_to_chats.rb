class AddMaxUsersToChats < ActiveRecord::Migration[5.2]
  def change
    add_column :chats, :max_users, :integer, default: 5, null: false
  end
end
