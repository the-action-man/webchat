class AddUniqIndexToUsersChats < ActiveRecord::Migration[5.2]
  def change
    add_index :users_chats, [:user_id, :chat_id], unique: true
  end
end
