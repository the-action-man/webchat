class AddMaxChatsToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :max_chats, :integer, default: 5, null: false
  end
end
