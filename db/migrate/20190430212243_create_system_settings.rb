class CreateSystemSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :system_settings do |t|
      t.integer :chats_count_for_user, default: 5, null: false
      t.integer :users_count_for_chat, default: 5, null: false
    end
  end
end
