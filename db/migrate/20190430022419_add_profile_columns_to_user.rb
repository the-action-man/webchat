class AddProfileColumnsToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :nickname, :string
    add_column :users, :birthday, :date
    add_column :users, :no_invitation, :boolean, default: false, null: false
  end
end
