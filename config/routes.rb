Rails.application.routes.draw do
  devise_for :users

  get '/users', to: 'users#index'
  get '/profile', to: 'users#edit'
  post '/profile', to: 'users#update'
  get '/users/:id/manage', to: 'users#manage', as: 'user_managing'
  post '/users/:id/apply_managing', to: 'users#apply_managing'
  delete '/users/:id/delete', to: 'users#destroy', as: 'delete_user'

  get '/black_list', to: 'blocked_users#index', as: 'black_list'
  get '/black_list/new', to: 'blocked_users#new', as: 'new_blocked_user'
  post '/black_list/add_user', to: 'blocked_users#add_user'
  delete '/blocked_user/:id/delete', to: 'blocked_users#destroy', as: 'delete_blocked_user'

  get 'chats/search', to: 'chats#search' # app interpreters 'search' as id. That is why it is on the first place
  resources :chats
  get '/chats/:id/users_list', to: 'chats#users_list', as: 'chat_users_list'
  post '/chats/:id/add_user', to: 'chats#add_user'
  post '/chats/:id/delete_user', to: 'chats#delete_user'
  get '/chats/:id/manage', to: 'chats#manage', as: 'chat_manging'
  post '/chats/:id/apply_managing', to: 'chats#apply_managing'
  post '/chats/:id/add_message', to: 'chats#add_message'

  get 'system_settings/edit', to: 'system_settings#edit', as: 'system_settings'
  post 'system_settings/update', to: 'system_settings#update'

  root to: "chats#index"
end
